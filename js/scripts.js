$(function() {
    var errorLogin = false;
    var errorPassword = false;

    $('.form-submit').on('click', function (e) {
        e.preventDefault();

        if (!errorLogin && !errorPassword && $('#login').val().length > 0 && $('#password').val().length > 0) {
            $('.error-login').html('&nbsp;');
            $('.error-password').html('&nbsp;');
            $('.login-form').hide();
            $('.confirm').show();

        } else {
            $('.error-login').html('Длинна поля login должна быть от 3 до 20 символов.');
            $('.error-password').html('Пароль должен содержать как минимум 1 большую букву, 1 маленькую, 1 цифру, 1 спец символ и иметь длинну от 5 до 20 символов');
        }
    });

    $('#login').on('keyup', function () {
        this.value = this.value.replace(/[^\w]/g, "");
        errorLogin = false;

        $('.error-login').html('&nbsp;');
        // проверка логин
        if ($('#login').val().length < 3 || $('#login').val().length > 20) {
            $('.error-login').html('Длинна поля login должна быть от 3 до 20 символов.');
            errorLogin = true;
        }
    });

    $('#password').on('keyup', function () {
        errorPassword = true;

        $('.error-password').html('Пароль должен содержать как минимум 1 большую букву, 1 маленькую, 1 цифру, 1 спец символ и иметь длинну от 5 до 20 символов');

        var reg = new RegExp ('(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{5,20}');

        if (reg.test(this.value)) {
            $('.error-password').html('&nbsp;');
            errorPassword = false;
        }

    });
});
